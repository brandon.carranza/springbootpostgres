package com.example.pricelist.controller;

import java.util.List;

import com.example.pricelist.TestResponse;
import com.example.pricelist.entity.Animal;
import com.example.pricelist.service.AnimalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnimalController{

    private final AnimalService animalService;
    private static final String template = "Hello, %s!";

    @Autowired
    public AnimalController(AnimalService animalService){
        this.animalService = animalService;
    }

    @RequestMapping("/animals")
    public List<Animal> getAll() {
        return animalService.getAll();
    }

    @GetMapping("/greeting")
	public TestResponse greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new TestResponse(1, String.format(template, name));
	}

}