package com.example.pricelist.dao;
import java.util.List;
import com.example.pricelist.entity.Animal;
public interface AnimalDao {

    List<Animal> findAll();

    Animal findById(long id);

    void insertAnimal(Animal animal);

    //void updateEmployee(Animal emp);
    //void executeUpdateEmployee(Animal emp);
    
    public void deleteAnimal(Animal animal);


}