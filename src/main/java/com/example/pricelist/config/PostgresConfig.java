package com.example.pricelist.config;

import com.example.pricelist.repository.postgres.AnimalRepositoryPostgresStrategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring {@link Configuration} class that creates the beans that allow the usage of PostgreSQL as database storage.
 */
@Profile("postgres")
@Configuration
@EnableJpaRepositories("com.example.pricelist.repository.postgres")
@EntityScan(basePackages = "com.example.pricelist.entity")
public class PostgresConfig {
    private final AnimalRepositoryPostgresStrategy animalRepositoryPostgresStrategy;

    /**
     * Constructor to inject the required dependencies
     *
     * @param priceListRepositoryPostgresStrategy    PostgreSQL enabled {@link com.salesforce.pricelist.repository.PriceListRepository}
     * @param editionPriceRepositoryPostgresStrategy PostgreSQL enabled {@link com.salesforce.pricelist.repository.EditionPriceRepository}
     */
    @Autowired
    public PostgresConfig(
        AnimalRepositoryPostgresStrategy animalRepositoryPostgresStrategy) {
        this.animalRepositoryPostgresStrategy = animalRepositoryPostgresStrategy;
    }

    /**
     * Getter for the property
     *
     * @return PostgreSQL enabled {@link com.salesforce.pricelist.repository.PriceListRepository}
     */
    @Bean(name = "animalRepository")
    public AnimalRepositoryPostgresStrategy getAnimalRepositoryPostgresStrategy() {
        return animalRepositoryPostgresStrategy;
    }
}
