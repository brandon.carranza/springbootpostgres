package com.example.pricelist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.example.pricelist")
// @EnableAutoConfiguration
public class PricelistApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricelistApplication.class, args);
	}

}
