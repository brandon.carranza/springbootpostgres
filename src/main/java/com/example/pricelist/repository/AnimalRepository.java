package com.example.pricelist.repository;

import java.util.List;

import com.example.pricelist.entity.Animal;


public interface AnimalRepository {

    List<Animal> getAll();
    
}