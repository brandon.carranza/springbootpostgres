package com.example.pricelist.repository.postgres;

import java.util.List;

import com.example.pricelist.entity.Animal;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaRepository} to query and store {@link PriceList} information
 */
@Profile("postgres")
public interface PostgresAnimalRepository extends JpaRepository<Animal, Long> {
    /**
     * Finds most recently uploaded price list by type
     *
     * @param type the type of the price list to look for
     * @return the most recent price list, based on its {@code uploadDate} property
     */
    List<Animal> findFirstByTypeOrderByUploadDateDesc();
}