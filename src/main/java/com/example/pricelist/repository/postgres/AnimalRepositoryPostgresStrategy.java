package com.example.pricelist.repository.postgres;

import com.example.pricelist.entity.Animal;
import com.example.pricelist.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.currentTimeMillis;

/**
 * {@link PriceListRepository} implementation that uses PostgreSQL as the
 * backing persistence mechanism.
 *
 * {@inheritDoc}
 */
@Component
@Profile("postgres")
public class AnimalRepositoryPostgresStrategy implements AnimalRepository {

    @Autowired
    private final PostgresAnimalRepository animalRepository;

    @Autowired
    public AnimalRepositoryPostgresStrategy(PostgresAnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @Override
    public List<Animal> getAll() {
        // TODO Auto-generated method stub
        return animalRepository.findFirstByTypeOrderByUploadDateDesc();
    }

    // @Override
    // @Transactional
    // public List<PriceList> saveAll(List<PriceList> priceLists, PriceListImportProcessStatusDto status) {
    //     List<PriceList> savedEntities = new ArrayList<>();
    //     long startSaving = currentTimeMillis();
    //     status.addLog(String.format("Saving %d price lists", priceLists.size()));
    //     for (PriceList priceList : priceLists) {
    //         long startSavingPriceList = currentTimeMillis();
    //         status.addLog(String.format("Saving price list '%s'", priceList.getType()));
    //         savedEntities.add(priceListRepository.save(priceList));
    //         status.addLog(String.format("Done saving price list '%s' (%d ms)", priceList.getType(),
    //                 currentTimeMillis() - startSavingPriceList));
    //     }
    //     status.addLog(String.format("Done saving price lists (%d ms)", currentTimeMillis() - startSaving));
    //     status.setFinished(true);
    //     return savedEntities;
    // }
}
