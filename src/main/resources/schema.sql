DROP TABLE IF EXISTS Animal;

CREATE TABLE Animal (
    id int PRIMARY KEY NOT NULL,
    name varchar(255),
    description varchar(255)
);